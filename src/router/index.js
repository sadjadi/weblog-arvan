import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    redirect: '/articles',
    meta: { requiresAuth: true },
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('../views/RegisterView.vue'),
    meta: { title: 'Register' }
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/LoginView.vue'),
    meta: { title: 'Login' }
  },
  {
    path: '/articles',
    name: 'articles',
    component: () => import('../views/ArticlesView.vue'),
    children: [
      { path: 'page/:page', name: 'article-pages', component: () => import('../views/ArticlesView.vue'), meta: { title: 'All Posts' } }
    ],
    meta: {
      title: 'All Posts',
      requiresAuth: true
    }
  },
  {
    path: '/articles/create',
    name: 'articles-create',
    component: () => import('../views/ArticleCreateView.vue'),
    meta: { 
      title: 'New Article',
      requiresAuth: true
    }
  },
  {
    path: '/articles/edit/:slug',
    name: 'articles-edit',
    component: () => import('../views/ArticleCreateView.vue'),
    meta: { 
      title: 'Edit Article' ,
      requiresAuth: true
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  let Store = new store
  if (to.name == 'login' || to.name == 'register') {
    if (!Store.state.user.access) {
      next()
    }
    else {
      next('/')
    }
  }
  else if (to.matched.some(record => record.meta.requiresAuth)) {
    if (Store.state.user.access){
      next()
    } else {
      next({ name: 'login' })
    }
  }
  else {
    next()
  }
})
const default_title = 'Weblog'
router.afterEach((to) => {
  Vue.nextTick(() => {
      document.title = `${default_title} | ${to.meta.title}`
  });
});

export default router
