import User from './User'
import Article from './Article'
import Tag from './Tag'

export default class Api {
  constructor() {
    this.User = new User()
    this.Article = new Article()
    this.Tag = new Tag()
  }
}