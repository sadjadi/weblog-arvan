import BaseService from '@/assets/js/base.service'
const api = new BaseService()

export default class User {
  constructor() {

  }
  register(data) {
    return api.post('users', data)
  }

  login(data) {
    return api.post('users/login', data)
  }
}