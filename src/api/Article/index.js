import BaseService from '@/assets/js/base.service'
const api = new BaseService()

export default class Article {
  constructor() {

  }
  get_article_per_page(page) {
    return api.get(`articles?limit=10&offset=${(page-1)*10}`)
  }

  get_an_article(data) {
    return api.get(`articles/${data}`)
  }

  update_an_article(slug, data) {
    return api.put(`articles/${slug}`, data)
  }

  create(data) {
    return api.post('articles', data)
  }

  delete(data) {
    return api.delete(`articles/${data}`)
  }

}