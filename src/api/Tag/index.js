import BaseService from '@/assets/js/base.service'
const api = new BaseService()

export default class Tag {
  constructor() {

  }
  get_all() {
    return api.get('tags')
  }

}