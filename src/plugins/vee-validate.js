import { required, email } from "vee-validate/dist/rules";
import { extend } from "vee-validate";

extend("required", {
  ...required,
  message: "Required {_field_}"
})

extend("email", {
  ...email,
  message: "This field must be a valid email"
})
