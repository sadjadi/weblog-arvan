import axios from 'axios'
import store_func from '../../store'

var BaseUrl = ''
export default class BaseService {
  constructor() {
    this.token = null
    BaseUrl = 'https://api.realworld.io/api/'
    axios.defaults.headers.post['Content-Type'] = 'application/json'
    axios.defaults.headers.post['Accept'] = 'application/json'
    axios.defaults.headers.post['Access-Control-Allow-Origin'] = window.location.origin
  }

  async get(url, data) {
    let Store = new store_func()
    this.token = Store.getters['user/get_token']

    return await axios.get(BaseUrl + url, {
      params: data,
      headers: {'Authorization': 'Bearer ' + this.token}
    })
    .then(response => {
      if (response.status == 200) {
        return response
      }
      else {
        console.log(response.data)
      }
    })
    .catch(async error => {
      if (error.response.status == 401) {
        console.log('need refresh token')
        return
      }
      console.log(error.response.data)
      return error.response
    })
  }

  async post(url, data) {
    let Store = new store_func()
    this.token = Store.getters['user/get_token']

    return await axios.post(BaseUrl + url, data, {
      headers: {'Authorization': 'Bearer ' + this.token}
    })
    .then(response => {
      if (response.status == 200) {
        return response
      }
      else {
        console.log(response.data)
      }
    })
    .catch(async error => {
      if (error.response.status == 401) {
        console.log('need refresh token')
        return
      }
      console.log('catch shode', error.response.data)
      return error.response
    })
  }

  async put(url, data) {
    let Store = new store_func()
    this.token = Store.getters['user/get_token']

    return await axios.put(BaseUrl + url, data, {
      headers: {'Authorization': 'Bearer ' + this.token}
    })
    .then(response => {
      if (response.status == 200) {
        return response
      }
      else {
        console.log('200 nashode', response.data)
      }
    })
    .catch(async error => {
      if (error.response.status == 401) {
        console.log('need refresh token')
        return
      }
      console.log('catch shode', error.response.data)
      return error.response
    })
  }

  async delete(url, data) {
    let Store = new store_func()
    this.token = Store.getters['user/get_token']

    return await axios.delete(BaseUrl + url, {headers: {Authorization: 'Bearer ' + this.token},data})
    .then(response => {
      if (response.status == 204) {
        return response
      }
      else {
        console.log(response.data)
      }
    })
    .catch(async error => {
      if (error.response.status == 401) {
        console.log('need refresh token')
        return
      }
      console.log(error.response.data)
      return error.response
    })
  }


}