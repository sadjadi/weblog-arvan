export const toast = {
  data() {
    return {
      toast_count: 0
    }
  },
  methods: {
    showToast(titleToast, bodyToast, variant, closebtn = false) {
      const h = this.$createElement
      const id = `my-toast-${this.toast_count++}`
      const vNodesTitle = h('div', { class: ['d-inline'] }, [h('strong', { class: 'mr-1' }, `${titleToast}`)])
      const vNodesMsg = h('div', { class: ['d-inline-flex', 'm-0', 'align-items-center'] }, [
        h('p', {class: ['d-inline', 'm-0']}, `${bodyToast}`),
        closebtn ? h('b-button', {on: { click: () => this.$bvToast.hide(id) }, class: 'btn-close-toast'}, 'x') : ''
      ])

      this.$bvToast.toast([vNodesMsg], {
        id: id,
        title: [vNodesTitle],
        solid: true,
        variant: variant,
        noAutoHide: false,
        noCloseButton: true
      })
    },
    showErrorApi(title, body) {
      this.$bvToast.toast(body, {
        title: title,
        solid: true,
        variant: 'danger',
        noAutoHide: false,
      })
    }
  }
}