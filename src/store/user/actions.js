export const set_email = ({ commit }, email) => {
  commit('set_email', email)
}

export const set_data_user = ({ commit }, data) => {
  commit('set_access', true)
  commit('set_bio', data.bio)
  commit('set_email', data.email)
  commit('set_image', data.image)
  commit('set_token', data.token)
  commit('set_username', data.username)
  
}

export const logout = ({ commit }) => {
  commit('set_access', false)
  commit('set_bio', null)
  commit('set_email', null)
  commit('set_image', null)
  commit('set_token', null)
  commit('set_username', null)
}