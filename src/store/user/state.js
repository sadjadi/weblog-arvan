export default function () {
  return {
    access: false,
    bio: null,
    email: null,
    image: null,
    token: null,
    username: null,
    // refresh_token: null
  }
}
