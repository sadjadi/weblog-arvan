export const set_access = (state, data) => {
  state.access = data
}
export const set_bio = (state, bio) => {
  state.bio = bio
}
export const set_email = (state, email) => {
  state.email = email
}
export const set_image = (state, image) => {
  state.image = image
}
export const set_token = (state, data) => {
  state.token = data
}
export const set_username = (state, username) => {
  state.username = username
}